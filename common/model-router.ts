import {Router} from './router'
import mongoose from 'mongoose'
import { NotFoundError } from 'restify-errors';

//classe abstrata parametrizada como Generics no tipo D
export abstract class ModelRouter <D extends mongoose.Document> extends Router {

    basePath: string

    pageSize: number = 4


    constructor(protected model: mongoose.Model<D>){
        super()
        this.basePath = `/${this.model.collection.name}`
    }

    //Prepara a query para um uso especializado, como popular itens por exemplo
    protected prepareOne(query: mongoose.DocumentQuery<D,D>): mongoose.DocumentQuery<D,D> {
        return query
    }

    //adiciona links e formata document para toJSON
    envelope(document :any): any{
        //faz uma cópia do recurso, o _ é para indicar que é metadados
        let resource = Object.assign({_links:{}}, document.toJSON())
        //adicionar o link ao item
        resource._links.self = `${this.basePath}/${resource._id}`
        return resource
    }

    //disponibilizar os links para navegação
    envelopeAll(documents: any[], options: any ={}): any {
        const resource: any = {
            _links:{
                self:`${options.url}`
            },
            items: documents
        }
        if(options.page && options.count && options.pageSize){
            if(options.page > 1){
                resource._links.previous = `${this.basePath}?_page=${options.page-1}`
            }
            const remaining = options.count - (options.page * options.pageSize)
            if(remaining > 0){
                resource._links.next = `${this.basePath}?_page=${options.page+1}`
            }
        }
        return resource
    }

    validarId = (req, resp, next)=> {
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            next(new NotFoundError('Documento não encontrado.'))
        } else {
            next()
        }
    }


    findAll = (req, resp, next)=> {
        let page = parseInt(req.query._page || 1)
        page = page > 0 ? page : 1

        const skip = (page - 1) * this.pageSize

        this.model.count({}).exec()
            .then(count => this.model.find()
                    .skip(skip)
                    .limit(this.pageSize)
                    .then(this.renderAll(resp, next, {
                        page, 
                        count, 
                        pageSize: this.pageSize, url: req.url
                    }))
            .catch(next)
        )
    }

    findById = (req, resp, next)=> {
        this.prepareOne(this.model.findById(req.params.id))
            .then(this.render(resp, next))
            .catch(next)
    }

    save = (req, resp, next) => {
        //criar um documento que vem via post
        let document = new this.model(req.body)
        document.save()
            .then(this.render(resp, next))
            .catch(next)
    }

    replace = (req, resp, next)=>{
        //para substituir o documento inteiro com overwrite
        const options = {runValidators:true, overwrite: true}
        this.model.update({_id:req.params.id}, req.body, options)
        .exec().then(result=>{
            if(result.n){ //valida se fez a alteração
                return this.model.findById(req.params.id)
            } else {
                resp.send(404)
            }
        }).then(this.render(resp, next))
        .catch(next)
    }

    update =  (req, resp, next)=> {
        const options = {runValidators:true, new : true} //para retornar os dados atualizados
        this.model.findOneAndUpdate(req.params.id, req.body, options)
            .then(this.render(resp, next))
            .catch(next)
    }

    delete = (req, resp, next)=>{
        this.model.deleteOne({_id:req.params.id}).exec()
        .then(cmdResult => {
            if(cmdResult.n){
                resp.send(204)
            } else {
                resp.send(404)
            }
            return next()
        })
        .catch(next)
    }
}