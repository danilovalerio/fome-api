import restify from 'restify'

//módulo que trata eventos
import {EventEmitter} from 'events'

//Class abstrata Router que se comportar similar ao Router do Express
export abstract class  Router extends EventEmitter {
    abstract applyRoutes(application: restify.Server) : any
    
    envelope(document :any): any{
        return document
    }

    //para criar links navegaveis das páginas
    envelopeAll(document: any[], options: any ={}) : any {
        return documents
    }

    //melhorar o reuso  das rotas
    render(response: restify.Response, next: restify.Next){
        return(documento) => {
            if(documento){
                this.emit('antesDaResposta', documento)
                response.json(this.envelope(documento))
            } else {
                response.send(404)
            }
            return next(false) //avisar que terminou como já passamos uma resposta
        }
    }

    //tratar da passagem de um array de documents
    renderAll(response: restify.Response, next: restify.Next, options: any ={}){
        return (documentos: any[]) => {
            if(documentos){
                documentos.forEach((documento, index, array) => {
                    this.emit('antesDaResposta', documento)
                    array[index] = this.envelope(documento)
                })
                response.json(this.envelopeAll(documentos, options))
            } else {
                response.json(this.envelopeAll([]))
            }
            return next(false)
        }
    }


}