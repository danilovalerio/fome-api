"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//configuração da porta, se não informar o padrão é 3000
exports.environment = {
    server: {
        port: process.env.SERVER_PORT || 3000
    },
    db: {
        url: process.env.DB_URL || 'mongodb://localhost/fome-api'
    },
    //configuração do número de ciclos para gerar o hash
    security: {
        saltRounds: process.env.SALT_ROUNDS || 10,
        apiSecret: process.env.API_SECRET || 'fome-api-secret',
        enableHTTPS: process.env.ENABLE_HTTPS || true,
        certificate: process.env.CERTI_FILE || './security/keys/cert.pem',
        key: process.env.CERT_KEY_FILE || './security/keys/key.pem'
    },
    log: {
        level: process.env.LOG_LEVEL || 'debug',
        name: 'fome-api-logger'
    }
};
