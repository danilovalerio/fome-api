"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//módulo que trata eventos
const events_1 = require("events");
//Class abstrata Router que se comportar similar ao Router do Express
class Router extends events_1.EventEmitter {
    envelope(document) {
        return document;
    }
    //para criar links navegaveis das páginas
    envelopeAll(document, options = {}) {
        return documents;
    }
    //melhorar o reuso  das rotas
    render(response, next) {
        return (documento) => {
            if (documento) {
                this.emit('antesDaResposta', documento);
                response.json(this.envelope(documento));
            }
            else {
                response.send(404);
            }
            return next(false); //avisar que terminou como já passamos uma resposta
        };
    }
    //tratar da passagem de um array de documents
    renderAll(response, next, options = {}) {
        return (documentos) => {
            if (documentos) {
                documentos.forEach((documento, index, array) => {
                    this.emit('antesDaResposta', documento);
                    array[index] = this.envelope(documento);
                });
                response.json(this.envelopeAll(documentos, options));
            }
            else {
                response.json(this.envelopeAll([]));
            }
            return next(false);
        };
    }
}
exports.Router = Router;
