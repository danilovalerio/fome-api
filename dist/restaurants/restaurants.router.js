"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const model_router_1 = require("../common/model-router");
const restaurants_model_1 = require("./restaurants.model");
const restify_errors_1 = require("restify-errors");
const authz_handler_1 = require("../security/authz.handler");
class RestaurantsRouter extends model_router_1.ModelRouter {
    constructor() {
        super(restaurants_model_1.Restaurant); //referência do model restaurants
        //callback para retornar o menu
        this.findMenu = (req, resp, next) => {
            //projeção é uma string que definimos o que vamos trazer
            restaurants_model_1.Restaurant.findById(req.params.id, "+menu")
                .then(restaurant => {
                if (!restaurant) {
                    throw new restify_errors_1.NotFoundError('Restaurante não encontrado.');
                }
                else {
                    resp.json(restaurant.menu);
                    return next();
                }
            }).catch(next);
        };
        this.replaceMenu = (req, resp, next) => {
            restaurants_model_1.Restaurant.findById(req.params.id)
                .then(restaurant => {
                if (!restaurant) {
                    throw new restify_errors_1.NotFoundError('Restaurante não encontrado.');
                }
                else {
                    restaurant.menu = req.body; //ARRAY de MenuItem
                    return restaurant.save();
                }
            }).then(restaurant => {
                resp.json(restaurant.menu);
                return next();
            }).catch(next);
        };
    }
    //para disponibilizar link para o menu
    envelope(document) {
        //copia do recurso
        let resource = super.envelope(document);
        resource._links.menu = `${this.basePath}/${resource._id}/menu`;
        return resource;
    }
    applyRoutes(application) {
        //callback's no model-router
        application.get(`${this.basePath}`, this.findAll);
        application.get(`${this.basePath}/:id`, [this.validarId, this.findById]);
        application.post(`${this.basePath}`, [authz_handler_1.authorize('admin'), this.save]);
        application.put(`${this.basePath}/:id`, [authz_handler_1.authorize('admin'), this.validarId, this.replace]);
        application.patch(`${this.basePath}/:id`, [authz_handler_1.authorize('admin'), this.validarId, this.update]);
        application.del(`${this.basePath}/:id`, [authz_handler_1.authorize('admin'), this.validarId, this.delete]);
        //rota que retorna o menu
        application.get(`${this.basePath}/:id/menu`, [this.validarId, this.findMenu]);
        //rota para alteração do menu do restaurante
        application.put(`${this.basePath}/:id/menu`, [authz_handler_1.authorize('admin'), this.validarId, this.replaceMenu]);
    }
}
exports.restaurantsRouter = new RestaurantsRouter();
