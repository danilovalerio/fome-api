"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const model_router_1 = require("../common/model-router");
const reviews_model_1 = require("./reviews.model");
const authz_handler_1 = require("../security/authz.handler");
class ReviewsRouter extends model_router_1.ModelRouter {
    constructor() {
        super(reviews_model_1.Review);
    }
    //para disponibilizar link para o restaurante no review
    envelope(document) {
        //copia do recurso
        let resource = super.envelope(document);
        //Verifica se tem id no restaurante se não passa somente document
        const restaurantId = document.restaurant._id ? document.restaurant._id : document.restaurant;
        resource._links.restaurant = `/restaurants/${restaurantId}`;
        return resource;
    }
    //Estratégia 2 findById para popular user.name e restaurant que só tem o nome mesmo
    prepareOne(query) {
        return query.populate('user', 'name')
            .populate('restaurant');
    }
    //OVERRIDE do findById para popular user.name e restaurant que só tem o nome mesmo
    /*Estratégia 1 Override
    findById = (req, resp, next)=> {
        this.model.findById(req.params.id)
            .populate('user','name')
            .populate('restaurant')
            .then(this.render(resp, next))
            .catch(next)
    }*/
    applyRoutes(application) {
        //callback's no model-router
        application.get(`${this.basePath}`, this.findAll);
        application.get(`${this.basePath}/:id`, [this.validarId, this.findById]);
        application.post(`${this.basePath}`, [authz_handler_1.authorize('user'), this.save]);
        // application.put(`${this.basePath}/:id`, [this.validarId, this.replace])
        // application.patch(`${this.basePath}/:id`,[this.validarId, this.update])
        // application.del(`${this.basePath}/:id`, [this.validarId, this.delete])
    }
}
exports.reviewsRouter = new ReviewsRouter();
