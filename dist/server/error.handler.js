"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleError = (req, resp, err, done) => {
    console.log(err);
    //restify procura a função toJSON
    err.toJSON = () => {
        return {
            message: err.message
        };
    };
    switch (err.name) {
        //Detalhando o erro que não é um 500 (erro interno do servidor) e sim um dado 
        //mal formatado, sendo assim, o cliente tem que tratar e mandar de volta
        //nosso server está ok, por isso mudamos o statusCode para 400 (bad request)
        case 'MongoError':
            if (err.code === 11000) {
                err.statusCode = 400;
            }
            break;
        case 'ValidationError':
            err.statusCode = 400;
            //exibir todos os erros de uma só vez
            const messages = [];
            for (let name in err.errors) {
                messages.push({ message: err.errors[name].message });
            }
            err.toJSON = () => ({
                message: 'Validação teve erro enquanto processava sua requisição.',
                errors: messages
            });
            break;
    }
    done();
};
