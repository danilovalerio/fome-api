"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs")); //para uso do https
const restify_1 = __importDefault(require("restify"));
const mongoose_1 = __importDefault(require("mongoose"));
const environment_1 = require("../common/environment");
const logger_1 = require("../common/logger");
const merge_patch_parser_1 = require("./merge-patch.parser");
const error_handler_1 = require("./error.handler"); //importando a callback
const token_parser_1 = require("../security/token.parser");
//Objeto Server
class Server {
    inicializeDb() {
        //metodo conect já retorna uma promise por isso chamamos direto
        mongoose_1.default.Promise = global.Promise;
        return mongoose_1.default.connect(environment_1.environment.db.url, { useMongoClient: true });
    }
    initRoutes(routers) {
        return new Promise((resolve, reject) => {
            try {
                //criar servidor
                const options = {
                    name: 'fome-api',
                    version: '1.0.0',
                    log: logger_1.logger
                };
                if (environment_1.environment.security.enableHTTPS) {
                    options.certificate = fs_1.default.readFileSync(environment_1.environment.security.certificate),
                        options.key = fs_1.default.readFileSync(environment_1.environment.security.key);
                }
                this.application = restify_1.default.createServer(options);
                this.application.pre(restify_1.default.plugins.requestLogger({
                    log: logger_1.logger
                }));
                //interpreta os valores via url num objeto query
                this.application.use(restify_1.default.plugins.queryParser());
                //para identificar a requisição e trasnformar em json
                this.application.use(restify_1.default.plugins.bodyParser());
                //para tratar dos dados via patch
                this.application.use(merge_patch_parser_1.mergePatchBodyParser);
                this.application.use(token_parser_1.tokenParser); //o token está disponível para todo request
                //rotas
                for (let router of routers) {
                    router.applyRoutes(this.application);
                }
                //escuta na porta 
                this.application.listen(environment_1.environment.server.port, () => {
                    resolve(this.application);
                });
                // this.application.get('/',(req, resp, next)=>{
                //     const rotas = routers
                // resp.json(rotas)
                // next()
                // })
                // this.application.get('/info', [
                //     //USO DO OBJETO NEXT 
                //     //mais de uma callback
                //     (req, resp, next) => {
                //         //interropendo a passagem para a próxima callback se o navegar foi IE 7
                //         if(req.userAgent() && req.userAgent().includes('MSIE 7.0')){
                //             // resp.status(400)
                //             // resp.json({message: 'Atualize seu brownser!'})
                //             let error: any = new Error()
                //             error.statusCode = 400
                //             error.message = 'Por favor atualize seu navegador.'
                //             return next(error) // ou return next(false) 
                //         }
                //         return next()
                //     },
                //     (req, resp, next) => {
                //         resp.json({
                //             browser:req.userAgent(),
                //             method: req.method,
                //             url: req.href(),
                //             path: req.path(),
                //             query: req.query,
                //         })
                //         return next()
                //     }]
                // )
                //tratar os erros com esse evento
                this.application.on('restifyError', error_handler_1.handleError);
                // this.application.on('after', restify.plugins.auditLogger({
                //     log: logger,
                //     event: 'after'
                // }))
            }
            catch (error) {
                reject(error);
            }
        });
    }
    bootstrap(routers = []) {
        //se conectar ao database, aí inicializa as rotas
        return this.inicializeDb().then(() => this.initRoutes(routers).then(() => this));
    }
    shutdown() {
        return mongoose_1.default.disconnect()
            .then(() => this.application.close());
    }
}
exports.Server = Server;
