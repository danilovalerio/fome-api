"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//rotas do recuso usuário
const model_router_1 = require("../common/model-router");
//importação do users model com dados em memória para teste
const users_model_1 = require("./users.model");
const auth_handler_1 = require("../security/auth.handler");
const authz_handler_1 = require("../security/authz.handler");
class UsersRouter extends model_router_1.ModelRouter {
    constructor() {
        super(users_model_1.User);
        //Implementação na vers 2.0.0 o findByEmail
        this.findByEmail = (req, resp, next) => {
            if (req.query.email) {
                users_model_1.User.findByEmail(req.query.email)
                    .then(user => {
                    if (user) {
                        return [user];
                    }
                    else {
                        return [];
                    }
                })
                    .then(this.renderAll(resp, next, {
                    pageSize: this.pageSize,
                    url: req.url
                }))
                    .catch(next);
            }
            else {
                next();
            }
        };
        //método que fica escutando quando tem chamada do evento
        this.on('antesDaResposta', documento => {
            documento.password = undefined;
            // documento.email = "@@@@@@@"
            //delete document.password
        });
    }
    applyRoutes(application) {
        //se tiver e-mail usa findByEmail, caso não passa para próxima
        //a busca da versão é feita pelo restify, quando não informada busca a mais recente
        application.get({ path: `${this.basePath}`, version: '2.0.0' }, [this.findByEmail, this.findAll]);
        application.get({ path: `${this.basePath}`, version: '1.0.0' }, [authz_handler_1.authorize('admin'), this.findAll]);
        application.get(`${this.basePath}/:id`, [authz_handler_1.authorize('admin'), this.validarId, this.findById]);
        application.post(`${this.basePath}`, [authz_handler_1.authorize('admin'), this.save]);
        application.put(`${this.basePath}/:id`, [authz_handler_1.authorize('admin'), this.validarId, this.replace]);
        application.patch(`${this.basePath}/:id`, [authz_handler_1.authorize('admin'), this.validarId, this.update]);
        application.del(`${this.basePath}/:id`, [authz_handler_1.authorize('admin'), this.validarId, this.delete]);
        application.post(`${this.basePath}/authenticate`, auth_handler_1.authenticate);
    }
}
//disponibiliza para os outros módulos que queiram utilizar
exports.usersRouter = new UsersRouter();
