import mongoose from 'mongoose'

export interface MenuItem extends mongoose.Document {
    name: string, 
    price: number
}

export interface Restaurant extends mongoose.Document {
    name: string, 
    menu: MenuItem[]
}

const menuSchema = new mongoose.Schema({
    name: {
        type: String, 
        required: true
    },
    price: {
        type: Number,
        required: true
    }
})

const restaurantSchema = new mongoose.Schema({
    name: {
        type: String, 
        required: true
    },
    menu: {
        type: [menuSchema], //tipos array de menus
        required: false, 
        select: false, //por padrão ao buscar restaurante não retorna menu
        default: []
    }
})

export const Restaurant = mongoose.model<Restaurant>('Restaurant',restaurantSchema)