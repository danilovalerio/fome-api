import {ModelRouter} from '../common/model-router'
import restify from 'restify'
import { Restaurant } from './restaurants.model';
import { NotFoundError } from 'restify-errors';

import {authorize} from '../security/authz.handler'

class RestaurantsRouter extends ModelRouter<Restaurant>{
    constructor(){
        super(Restaurant) //referência do model restaurants
    }

    //para disponibilizar link para o menu
    envelope(document){
        //copia do recurso
        let resource = super.envelope(document)
        resource._links.menu = `${this.basePath}/${resource._id}/menu`
        return resource
    }

    //callback para retornar o menu
    findMenu = (req, resp, next) => {
        //projeção é uma string que definimos o que vamos trazer
        Restaurant.findById(req.params.id, "+menu")
            .then(restaurant => {
                if(!restaurant){
                    throw new NotFoundError('Restaurante não encontrado.')
                } else {
                    resp.json(restaurant.menu)
                    return next()
                }
            }).catch(next)
    }

    replaceMenu = (req, resp, next) => {
        Restaurant.findById(req.params.id)
            .then(restaurant => {
                if(!restaurant){
                    throw new NotFoundError('Restaurante não encontrado.')
                } else {
                   restaurant.menu = req.body //ARRAY de MenuItem
                   return restaurant.save()
                }
            }).then(restaurant => {
                resp.json(restaurant.menu)
                return next()
            }).catch(next)
    }

    applyRoutes(application: restify.Server){
        //callback's no model-router
        application.get(`${this.basePath}`, this.findAll)
        application.get(`${this.basePath}/:id`, [this.validarId, this.findById])
        application.post(`${this.basePath}`, [authorize('admin'),this.save])
        application.put(`${this.basePath}/:id`, [authorize('admin'),this.validarId, this.replace])
        application.patch(`${this.basePath}/:id`,[authorize('admin'),this.validarId, this.update])
        application.del(`${this.basePath}/:id`, [authorize('admin'),this.validarId, this.delete])

        //rota que retorna o menu
        application.get(`${this.basePath}/:id/menu`,[this.validarId, this.findMenu])
        //rota para alteração do menu do restaurante
        application.put(`${this.basePath}/:id/menu`,[authorize('admin'),this.validarId, this.replaceMenu])
    }
}

export const restaurantsRouter = new RestaurantsRouter()