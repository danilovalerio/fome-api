import mongoose from 'mongoose'
import { Restaurant } from '../restaurants/restaurants.model';
import { User } from '../users/users.model.Antigo';

//interface de review
export interface Review extends mongoose.Document {
    date: Date,
    rating: string,
    restaurant: mongoose.Types.ObjectId | Restaurant, //pode ser ObjectId ou o próprio Document
    user: mongoose.Types.ObjectId | User,
}

const reviewSchema = new mongoose.Schema({
    date: {
        type: Date,
        required: true
    },
    rating: {
        type: Number,
        required: true
    },
    comments: {
        type: String,
        required: true,
        maxlength: 500
    },
    restaurant: {
        type: mongoose.Schema.Types.ObjectId, //associa o objId 
        ref: 'Restaurant', //modelo que será referência
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId, //associa o objId 
        ref: 'User', //modelo que será referência
        required: true
    }

})

export const Review = mongoose.model<Review>('Review', reviewSchema)