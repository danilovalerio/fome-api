import 'jest'
import request from 'supertest'

let address: string = (<any>global).address

//realizar o teste do get
test('get /reviews', ()=>{
    return request(address)
        .get('/reviews')
        .then(response =>{
            expect(response.status).toBe(200) //espero que o status seja 200
            expect(response.body.items).toBeInstanceOf(Array) //esperamos um array de objetos
        }).catch(fail)
})