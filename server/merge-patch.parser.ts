import restify from 'restify'
import {BadRequestError} from 'restify-errors'

//para suportar a conversão de dados corretamente quando utilizado o patch 
//para atualização parcial de dados ao invés do put
const mpContentType = 'application/merge-patch+json'

export const mergePatchBodyParser  = (req: restify.Request, resp: restify.Response, next : restify.Next) => {
    if(req.getContentType() === mpContentType && req.method === 'PATCH'){
        (<any>req).rawBody = req.body
        try {
            req.body = JSON.parse(req.body)
        } catch (e) {
            return next(new BadRequestError(`Conteúdo invalido: ${e.message}`))
        }
        return next()
    }

}