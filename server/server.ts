import fs from 'fs' //para uso do https

import restify from 'restify'
import mongoose from 'mongoose'

import {environment} from '../common/environment'
import {logger} from '../common/logger'
//Router
import {Router} from '../common/router'
import { mergePatchBodyParser } from './merge-patch.parser';
import {handleError} from './error.handler' //importando a callback

import {tokenParser} from '../security/token.parser'

//Objeto Server
export class Server {

    application: restify.Server

    inicializeDb(): mongoose.MongooseThenable {
        //metodo conect já retorna uma promise por isso chamamos direto
        (<any>mongoose).Promise = global.Promise
        return mongoose.connect(environment.db.url,{useMongoClient: true})
    }

    initRoutes(routers: Router[]): Promise<any>{
        return new Promise((resolve, reject) => {
            try{                
                //criar servidor
                const options: restify.ServerOptions = {
                    name: 'fome-api',
                    version: '1.0.0',
                    log: logger
                }

                if(environment.security.enableHTTPS){
                    options.certificate = fs.readFileSync(environment.security.certificate),
                    options.key = fs.readFileSync(environment.security.key)
                }
          
                this.application = restify.createServer(options)
          
                this.application.pre(restify.plugins.requestLogger({
                    log: logger
                }))
          

                //interpreta os valores via url num objeto query
                this.application.use(restify.plugins.queryParser())
                //para identificar a requisição e trasnformar em json
                this.application.use(restify.plugins.bodyParser())
                //para tratar dos dados via patch
                this.application.use(mergePatchBodyParser)
                this.application.use(tokenParser) //o token está disponível para todo request

                //rotas
                for(let router of routers){
                    router.applyRoutes(this.application)
                }

                //escuta na porta 
                this.application.listen(environment.server.port, ()=>{
                    resolve(this.application)
                 })

                // this.application.get('/',(req, resp, next)=>{
                //     const rotas = routers



                    // resp.json(rotas)
                    // next()
                // })

                // this.application.get('/info', [
                //     //USO DO OBJETO NEXT 
                //     //mais de uma callback
                    
                //     (req, resp, next) => {
                //         //interropendo a passagem para a próxima callback se o navegar foi IE 7
                //         if(req.userAgent() && req.userAgent().includes('MSIE 7.0')){
                //             // resp.status(400)
                //             // resp.json({message: 'Atualize seu brownser!'})

                //             let error: any = new Error()
                //             error.statusCode = 400
                //             error.message = 'Por favor atualize seu navegador.'
                //             return next(error) // ou return next(false) 
                //         }
                //         return next()
                //     },
                //     (req, resp, next) => {
                //         resp.json({
                //             browser:req.userAgent(),
                //             method: req.method,
                //             url: req.href(),
                //             path: req.path(),
                //             query: req.query,
                //         })
                //         return next()

                //     }]
                // )

                //tratar os erros com esse evento
                this.application.on('restifyError', handleError)
                // this.application.on('after', restify.plugins.auditLogger({
                //     log: logger,
                //     event: 'after'
                // }))


            } catch(error){
                reject(error)
            }
        })
    }
    
    bootstrap(routers: Router[] = []): Promise<Server>{
        //se conectar ao database, aí inicializa as rotas
        return this.inicializeDb().then(()=> 
               this.initRoutes(routers).then(() => this))   
    }

    shutdown(){
        return mongoose.disconnect()
                        .then(()=>this.application.close())
    }
}