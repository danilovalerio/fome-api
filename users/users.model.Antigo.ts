//arquivo com dados em memória antes de usar o mongoose só para termos dados
const users =[
    {id: '1', name:'Joao Joana', email:'jj@gmail.com'},
    {id: '2',name:'Alan Alana', email:'aa@gmail.com'}
]

//emular como se estivessemos acessando ou fazendo consulta ao db
export class User{
    static findAll(): Promise<any[]>{
        return Promise.resolve(users)
    }

    static findById(id: string): Promise<any>{
        return new Promise(resolve=>{
            const filtrado = users.filter(user=> user.id === id)
            let user = undefined
            if(filtrado.length > 0){
                user = filtrado[0]
            }
            resolve(user)
        })
    }
}