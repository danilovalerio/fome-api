import mongoose from 'mongoose'
import {validateCPF} from '../common/validadores'
import bcrypt, { hash } from 'bcrypt'
import {environment } from '../common/environment'

//interface para facilitar criação de objetos User e autocomplete
export interface User extends mongoose.Document {
    name: string,
    email: string,
    password: string,
    cpf: string,
    gender: string,
    profiles: string[], //perfis que irão determinar niveis de acesso
    matches(password: string) : boolean,
    hasAny(...profiles: string[]): boolean
    //hasAny('admin', 'user')
}

//interface para representar o model 
export interface UserModel extends mongoose.Model<User>{
    findByEmail(email: string, projection?: string): Promise<User>
}

//schema que representa user e seus metadados
const userSchema = new mongoose.Schema({
    name:{
        type:String, //objeto string do Javascript
        required: true, //validação do mongoose
        maxlength: 80,
        minlength: 3
    },
    email: {
        type:String,
        unique: true, //será unico
        required: true,
        //expressão regex para validar e-mail com o match do mongoose
        match: /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i

    },
    password: {
        type: String,
        select: false,
        required: true
    },
    gender: {
        type: String,
        required: false,
        enum: ['M','F']
    },
    cpf: {
        type: String,
        required: false, 
        //validador personalizado com o validate do mongoose
        validate: {
            validator: validateCPF,
            message: '{PATH}: CPF inválido ({VALUE})'
        }
    },
    profiles :{
        type: [String],
        required: false
    }

})

//Método personalizado em model, esse é estático, pois temos o documento inteiro
userSchema.statics.findByEmail = function(email: string, projection: string){
    return this.findOne({email}, projection) //{email: email}
}

userSchema.methods.matches = function(password: string) :boolean{
    return bcrypt.compareSync(password, this.password)
}

userSchema.methods.hasAny = function(...profiles: string[]) : boolean {
    return profiles.some(profile => this.profiles.indexOf(profile)!== -1)
  }

const hashPassword = (obj, next) => {
    bcrypt.hash(obj.password, environment.security.saltRounds)
            .then(hash => {
                obj.password = hash
                next()
            }).catch(next)
}

const saveMiddleware = function(next) {
    const user: User = this 
    if(!user.isModified('password')){
        next()
    } else {
        hashPassword(user, next)
    }

}

const updateMiddleware = function(next) {
    if(!this.getUpdate().password){
        next()
    } else {
       hashPassword(this.getUpdate(), next)
    }
}

userSchema.pre('save', saveMiddleware)
userSchema.pre('findOndeAndUpdate', updateMiddleware)
userSchema.pre('update', updateMiddleware)


//tipifica a iterface para o model ser do tipo User
export const User = mongoose.model<User, UserModel>('User', userSchema)