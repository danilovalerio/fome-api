//rotas do recuso usuário
import {ModelRouter} from '../common/model-router'
import restify from 'restify'
//importação do users model com dados em memória para teste
import { User } from './users.model';

import {authenticate} from '../security/auth.handler'
import {authorize} from '../security/authz.handler'

class UsersRouter extends ModelRouter<User>{

    constructor(){
        super(User)
        //método que fica escutando quando tem chamada do evento
        this.on('antesDaResposta', documento => {
            documento.password = undefined  
            // documento.email = "@@@@@@@"
            //delete document.password
        })
    }

    //Implementação na vers 2.0.0 o findByEmail
    findByEmail = (req, resp, next)=> {
        if(req.query.email){
            User.findByEmail(req.query.email)
                .then(user => {
                    if(user){
                        return [user]
                    } else {
                        return []
                    }
                })
                .then(this.renderAll(resp, next,{
                    pageSize: this.pageSize,
                    url: req.url
                }))
                .catch(next)
        } else {
            next()
        }
    }


    applyRoutes(application: restify.Server){

        //se tiver e-mail usa findByEmail, caso não passa para próxima
        //a busca da versão é feita pelo restify, quando não informada busca a mais recente
        application.get({path:`${this.basePath}`, version: '2.0.0'}, [this.findByEmail, this.findAll])
        application.get({path:`${this.basePath}`, version: '1.0.0'},[authorize('admin'), this.findAll])
        application.get(`${this.basePath}/:id`, [authorize('admin'),this.validarId, this.findById])
        application.post(`${this.basePath}`, [authorize('admin'),this.save])
        application.put(`${this.basePath}/:id`, [authorize('admin'),this.validarId, this.replace])
        application.patch(`${this.basePath}/:id`,[authorize('admin'),this.validarId, this.update])
        application.del(`${this.basePath}/:id`, [authorize('admin'),this.validarId, this.delete])

        application.post(`${this.basePath}/authenticate`, authenticate)
    }
}

//disponibiliza para os outros módulos que queiram utilizar
export const usersRouter = new UsersRouter()