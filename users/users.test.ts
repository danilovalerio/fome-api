import 'jest'
import request from 'supertest'

//para usar um ambiente diferente só para testes
import {Server} from '../server/server'
import {environment} from '../common/environment'
import {usersRouter} from '../users/users.router'
import {User} from './users.model'

let address: string = (<any>global).address

//realizar o teste do get
test('get /users', ()=>{
    return request(address)
        .get('/users')
        .then(response =>{
            expect(response.status).toBe(200) //espero que o status seja 200
            expect(response.body.items).toBeInstanceOf(Array) //esperamos um array de objetos
        }).catch(fail)
})

test('post /users',()=>{
    return request(address)
        .post('/users')
        .send({
            name: 'usuario1',
            email: 'user1@email.com',
            password: '123456',
            cpf: '962.116.531-82'
        })
        .then(response =>{
            expect(response.status).toBe(200) //espero que o status seja 200
            expect(response.body._id).toBeDefined()
            expect(response.body.name).toBe('usuario1')
            expect(response.body.email).toBe('user1@email.com')
            expect(response.body.cpf).toBe('962.116.531-82')
            expect(response.body.password).toBeUndefined()
        }).catch(fail)
})

test('get /users/aaa - not found', ()=>{
    return request(address)
    .get('/users/aaa')
    .then(response =>{
        expect(response.status).toBe(404)
    }).catch(fail)
})

test('patch /users/:id', ()=> {
    return request(address)
        .post('/users')
        .send({
            name: 'usuario2',
            email: 'user2@email.com',
            password: '123456'
        })
        .then(response => request(address)
                            .patch(`/users/${response.body._id}`)
                            .send({
                                name: 'usuario2 - patch'
                            }))
        .then(response => {
            expect(response.status).toBe(200) //espero que o status seja 200
            expect(response.body._id).toBeDefined()
            expect(response.body.name).toBe('usuario2 - patch')
            expect(response.body.email).toBe('user2@email.com')
            expect(response.body.password).toBeUndefined()

        })
        .catch(fail)
})
